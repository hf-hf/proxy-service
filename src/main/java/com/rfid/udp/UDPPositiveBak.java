//package com.rfid.udp;
//
//import java.io.IOException;
//import java.net.DatagramPacket;
//import java.net.DatagramSocket;
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//
//import org.springframework.util.StringUtils;
//
//import com.rfid.utils.UDPTool;
//
//import lombok.extern.slf4j.Slf4j;
//
///**
// * UDP主动发送
// *
// * 1.  明确对方的端口号， 向对方主动发送消息, 如心跳消息
// * 2.  不断接受到对方的回应
// */
//@Slf4j
//public class UDPPositiveBak {
//
//    private UDPPositiveBak proxy;
//
//	private DatagramSocket socket;
//
//    private String host;
//
//    private int port;
//
//    private static final byte[] SEND_DATA_IP = {(byte)255,(byte)255,(byte)255,(byte)255};
//
//    public Integer RECEIVE_PORT = null;
//
//    public UDPPositiveBak() {
//        super();
//    }
//
//    public UDPPositiveBak(int port){
//        this.port = port;
//        init();
//    }
//
//    public UDPPositiveBak(int port, String ip){
//        this.host = ip;
//        this.port = port;
//        init();
//    }
//
//    public void setProxy(UDPPositiveBak proxy) {
//        this.proxy = proxy;
//    }
//
//    public void init(){
//        try {
//            InetAddress add = getAddress();
//            this.socket = new DatagramSocket(port, add);
//            log.debug("端口：{}，开启监听成功！", port);
//        } catch (Exception e) {
//            log.error("绑定udp端口异常！");
//        }
//    }
//
//    public InetAddress getAddress() throws UnknownHostException {
//        InetAddress add;
//        if(StringUtils.isEmpty(host)){
//            add = InetAddress.getLocalHost();//UDPTool.getLocalHostLANAddress();
//        } else {
//            add = InetAddress.getByName(host);
//        }
//        log.info("端口：{}，地址：{}", port, add.toString());
//        return add;
//    }
//
//    /**
//     * 接收线程
//     */
//    public void process() {
//        new Thread(() -> {
//            try {
//                while (true) {
//                    try {
//                        receiveData();
//                    } catch (Exception e){
//                        log.error("接收线程退出{}",e.getMessage());
//                    }
//                }
//            } finally {
//                if(null != socket){
//                    socket.close();
//                }
//            }
//        }).start();
//    }
//
//    /**
//     * 接收数据
//     */
//    protected void receiveData(){
//        try {
//            //Map<String, String> result = new HashMap<String, String>();
//            byte[] buffer = new byte[1024];
//            DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);
//            log.debug("端口：{}，---开始接收--", this.port);
//            socket.receive(packet);
//            //添加数据解析方法
//            int len = packet.getLength();
//            byte[] data = packet.getData();
//            if (len != 0) {
//                byte[] send = new byte[len];
//                System.arraycopy(data, 0, send, 0, len);
//                RECEIVE_PORT = packet.getPort();
//                String hexString = UDPTool.toHexString(data, len);
//                String time = UDPTool.getCurrentTime();
//                log.debug("端口：{}，---接收到服务端的时间和数据--{}，内容:{}",
//                        this.port, time ,hexString);
//                proxy.sendData(send);
//                //result.put("message", "---接收到服务端的时间和数据--" + time + "内容:" + hexString);
//            }
//        } catch (IOException e) {
//            log.error("接收消息异常！", e);
//        }
//    }
//
//    /**
//     * 发送消息[这里以心跳为例]
//     */
//    public void sendUdpData(byte[] data){
//    	//UDPTool.sendData(serverIP, serverPort, data, socket);
//        log.debug(socket.getLocalPort() + "本地端口");
//    }
//
//    /**
//     * udp send data
//     * @param data
//     * @throws IOException
//     */
//    public void sendData(byte[] data) throws IOException {
//        if(null == RECEIVE_PORT){
//            throw new RuntimeException("指令发送失败，连接异常！");
//        }
//        DatagramPacket packet = new DatagramPacket(data, data.length,
//                InetAddress.getByAddress(SEND_DATA_IP), RECEIVE_PORT);
//        log.debug("转发数据到端口：{}", RECEIVE_PORT);
//        this.socket.send(packet);
//    }
//
//}
