package com.rfid.udp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Integer.MIN_VALUE)
public class UdpBootstrap implements ApplicationRunner {

    @Value("${udp.port}")
    private Integer udpPort;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        UDPPositive udpPositive = new UDPPositive(udpPort);
        udpPositive.process();
    }

}
